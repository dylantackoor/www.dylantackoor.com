const isStrictMode = false

module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier',
    'prettier/@typescript-eslint',
    'prettier/react',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  ignorePatterns: ['public/sw.js*', 'public/workbox-*.js*'],
  plugins: ['react', '@typescript-eslint'],
  rules: {
    'react/prop-types': 'off',
    'linebreak-style': ['error', 'unix'],
    '@typescript-eslint/no-unused-vars': isStrictMode ? 'error' : 'warn',
    'no-unneeded-ternary': 'error',
    'no-warning-comments': isStrictMode
      ? ['error', { terms: ['fixme'], location: 'anywhere' }]
      : 'warn',
  },
}
