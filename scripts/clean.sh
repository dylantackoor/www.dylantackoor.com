#!/bin/bash

cd ..

sudo rm -rf mariadb packages/server/build packages/server/dist packages/server/src/routes.ts packages/api/dist
docker system prune --volumes
docker image prune -a
yarn clean
rm -rf node_modules
yarn cache clean
