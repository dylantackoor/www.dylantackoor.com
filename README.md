# @dylantackoor/\* monorepo

A yarn workspace containing a node web client and server + utilities. Currently playing with `react-three-fiber` and `react-xr`.

Full Package list:

- [@dylantackoor/www](./packages/www/package.json) (NextJS web client)
- [@dylantackoor/server](./packages/server/package.json) (Express REST/GraphQL server)
- [@dylantackoor/interfaces](./packages/interfaces/package.json) (TypeScript interfaces shared between www & server)
- [@dylantackoor/eslint-config-dylantackoor](./packages/eslint-config-dylantackoor/package.json) (shared lint configs)

## Quick Start

Install packages

```sh
nvm use
npm i -g yarn
yarn install
```

Run Server ([README](./packages/server/README.md)) and Client ([README](./packages/www/README.md))

```sh
docker-compose up --build -d
```

## Workspace Wide Commands

Commands at the root/workspace level typically use lerna to execute a script of the same name in each package.

```sh
yarn clean # cleans cache/build/dist folders, then all node_modules
yarn format # prettier formats all js,ts,jsx,tsx,json,yml,yaml
yarn lint # eslint against all js,ts,jsx,tsx
yarn stylelint # Validates styles for css,jsx,tsx
yarn types # runs tsc in each package
yarn test # runs jest in each package
yarn markdownlint # lints .md files using workspace [config](./.markdownlint.json)
```

## TODOs

- [x] Setup Workspaces w/example shared @dylantackoor/\* usage
- [x] Configure all code quality tools
  - [x] markdownlint
  - [x] prettier
  - [x] per package stylelint configs + shared base + workspace wide script
  - [x] per package tsconfig + shared base + workspace wide script
  - [x] per package eslint configs
  - [x] package for shared eslint config
- [x] Configure commit hooks
- [ ] import react native app example
- [ ] Docker
  - [x] compose file with database
  - [x] Dockerfile for api
  - [ ] Dockerfile for www
- [ ] check if the new Husky hook package lets you run scripts based on staged file location
  - [ ] do ^
  - [ ] remove workspace level eslint config
- [ ] reconfigure vscode debuggers
  - [ ] figure out how to debug docker containers
  - [ ] www debug config
  - [ ] api debug config
