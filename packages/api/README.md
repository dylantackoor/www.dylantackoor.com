# @dylantackoor/api

An auto-generated TypeScript package for communicating with the server.

Intentionally empty in source control as these files will be generated to ensure everything is up to date.

## TODOs

- [ ] add a baseURL to tsoa (like `/v1/`) to get this working
