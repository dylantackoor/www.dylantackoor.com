# <https://dlan.me>

A static web client for messing around with webxr.

- Next.js but not server rendering anything
- [Redux Toolkit](https://redux-toolkit.js.org/) for state management
- Includes [Storybook](http://storybook.js.org/)
- [NetlifyCMS](https://www.netlifycms.org/) configured for future use (why cats.md exists)
- WASD game controls + VR mode
- currently uses [gltfjsx](https://github.com/pmndrs/gltfjsx) for loading 3d models

## TODOs

- [ ] integrate with `http://localhost:3000/auth`
- [ ] decide on ThreeJS vs Babylon (currently liking Babylon more i think)
