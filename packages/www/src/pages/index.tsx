import React, { FunctionComponent } from 'react'
import { Person } from 'schema-dts'
import { jsonLdScriptProps } from 'react-schemaorg'

import { SEO } from '@/components/dom'
import { HelloWorldLevel } from '@/components/canvas/levels'

const WebXRPage: FunctionComponent = () => (
  <>
    <SEO title="Dylan's WebXR Page">
      <script
        {...jsonLdScriptProps<Person>({
          '@context': 'https://schema.org',
          '@type': 'Person',
          name: 'Dylan Tackoor',
          url: 'https://www.dylantackoor.com',
        })}
      />
    </SEO>
    <div style={{ height: '100vh', width: '100vw' }}>
      <HelloWorldLevel />
    </div>
  </>
)

export default WebXRPage
