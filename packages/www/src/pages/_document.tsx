import React from 'react'
// import crypto from 'crypto'
import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentInitialProps,
  DocumentContext,
} from 'next/document'

// const cspHashOf = (text: string): string => {
//   const hash = crypto.createHash('sha256')
//   hash.update(text)
//   return `'sha256-${hash.digest('base64')}'`
// }

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext): Promise<DocumentInitialProps> {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render(): JSX.Element {
    // let csp = `default-src 'self'; script-src 'self' ${cspHashOf(
    //   NextScript.getInlineScriptSource(this.props),
    // )}`
    // if (process.env.NODE_ENV !== 'production') {
    //   csp = `style-src 'self' 'unsafe-inline'; font-src 'self' data:; default-src 'self'; script-src 'unsafe-eval' 'self' ${cspHashOf(
    //     NextScript.getInlineScriptSource(this.props),
    //   )}`
    // }

    return (
      <Html>
        <Head>
          <link type="text/plain" rel="author" href="/humans.txt" />
          {/*<meta httpEquiv="Content-Security-Policy" content={csp} />*/}
        </Head>
        <body style={{ margin: 0 }}>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
