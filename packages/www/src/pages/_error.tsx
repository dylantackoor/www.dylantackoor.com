import React from 'react'
import { NextApiRequest, NextApiResponse } from 'next'

const ErrorPage = ({ statusCode }: { statusCode: string }) => (
  <p>{statusCode ? `An error ${statusCode} occurred on server` : 'An error occurred on client'}</p>
)

ErrorPage.getInitialProps = ({ res, err }: { res: NextApiRequest; err: NextApiResponse }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return { statusCode }
}

export default Error
