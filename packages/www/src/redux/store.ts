import { configureStore, combineReducers, Action } from '@reduxjs/toolkit'
import { useDispatch } from 'react-redux'
import { ThunkAction } from 'redux-thunk'
import { authReducer } from './reducers/authSlice'

// const preloadedStateCache = localStorage.getItem('stateCache')
// if (preloadedStateCache) {
//   try {
//     storeOptions.preloadedState = JSON.parse(preloadedStateCache)
//   } catch (error) {
//     const err: Error = error
//     console.error(`State cache hydration error:`, err.message)
//   }
// }

export const store = configureStore({
  devTools: process.env.NODE_ENV === 'production',
  reducer: combineReducers({
    auth: authReducer,
  }),
})

export type AppState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export type AppThunk = ThunkAction<void, AppState, null, Action<string>>
export const useAppDispatch = () => useDispatch<AppDispatch>() // Export a hook that can be reused to resolve types
