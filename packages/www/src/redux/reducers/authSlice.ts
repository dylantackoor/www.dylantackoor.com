import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import {
  JSONResponseError,
  User,
  UserPostRequest,
  UserPostResponse,
} from '@dylantackoor/interfaces'

import { AppState } from '../store'
import { callApi } from '../api'

type AuthState = {
  loading: 'idle' | 'pending' | 'succeeded' | 'failed'
  user: Omit<User, 'password'>
  errors: JSONResponseError[]
}

const initialState: AuthState = {
  loading: 'idle',
  errors: [],
  user: {
    id: -1,
    email: '',
    name: '',
  },
}

export const registerUser = createAsyncThunk(
  'USER/REGISTER',
  async (opts: UserPostRequest, { rejectWithValue }) => {
    try {
      const res = await callApi<UserPostResponse>('/auth', {
        method: 'POST',
        body: JSON.stringify(opts),
        headers: {
          'content-type': 'application/json;charset=UTF-8',
        },
      })

      if (res.errors) {
        const errs: JSONResponseError[] = res.errors
        rejectWithValue(errs)
      }

      return res.data
    } catch (error) {
      // TODO: replace with proper logging solution
      console.error(error)
      return rejectWithValue(error.meta)
    }
  },
)

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setSignOut: state => {
      state = initialState
    },
  },
  extraReducers: builder => {
    builder.addCase(registerUser.pending, state => {
      state.loading = 'pending'
    })
    builder.addCase(registerUser.fulfilled, (state, action) => {
      state.loading = 'succeeded'
      state.user = action.payload as Omit<User, 'password'>
    })
    builder.addCase(registerUser.rejected, state => {
      state.loading = 'failed'
    })
  },
})

export const authReducer = authSlice.reducer
export const authSelector = (state: AppState): AuthState => state.auth
export const { setSignOut } = authSlice.actions
