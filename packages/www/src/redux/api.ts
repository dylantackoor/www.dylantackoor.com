import { JSONResponse } from '@dylantackoor/interfaces'

export async function callApi<T>(path: string, opts: RequestInit): Promise<JSONResponse<T>> {
  const url = `${process.env.REACT_NATIVE_API_BASE_URL}${path}`

  // TODO: actually just implement JWT auth here
  const token = ''
  if (token) {
    if (opts.headers) {
      opts.headers = {
        'basic-auth': '',
        ...opts.headers,
      }
    } else {
      opts.headers = {
        'basic-auth': '',
      }
    }
  }

  const data = await fetch(url, opts)
  const res: JSONResponse<T> = await data.json().catch(
    (fetchErr: Error): JSONResponse<T> => ({
      errors: [
        {
          message: fetchErr.message,
        },
      ],
    }),
  )

  return res
}
