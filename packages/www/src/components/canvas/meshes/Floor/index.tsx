import React, { FunctionComponent } from 'react'
import { Plane } from '@react-three/drei'
import { usePlane, PlaneProps } from '@react-three/cannon'

export interface FloorProps extends PlaneProps {
  color: string
}

export const Floor: FunctionComponent<FloorProps> = props => {
  const [ref] = usePlane(() => ({ rotation: [-Math.PI / 2, 0, 0], type: 'Static', ...props }))

  return (
    <Plane ref={ref} receiveShadow>
      <meshStandardMaterial attach="material" color={props.color} />
      <planeBufferGeometry attach="geometry" args={[100, 100]} />
    </Plane>
  )
}
