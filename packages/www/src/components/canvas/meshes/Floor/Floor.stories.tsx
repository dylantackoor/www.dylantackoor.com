import React from 'react'
import { Meta, Story } from '@storybook/react/types-6-0'

import { Floor, FloorProps } from '.'
import { LevelLayout, PreviewLayout } from '@/components/canvas'

export default {
  title: 'Objects/Floor',
  component: Floor,
} as Meta

const Template: Story<FloorProps> = args => <Floor {...args} />

export const Preview = Template.bind({})
Preview.decorators = [
  Story => (
    <PreviewLayout camera={{ position: [0, 2, 10] }}>
      <Story />
    </PreviewLayout>
  ),
]

export const Level: Story<FloorProps> = () => (
  <LevelLayout>
    <></>
  </LevelLayout>
)
