import React from 'react'
import { Meta, Story } from '@storybook/react/types-6-0'

import { Geo, GeoProps } from '.'
import { LevelLayout, PreviewLayout } from '@/components/canvas'

export default {
  title: 'Objects/Geo',
  component: Geo,
  args: {
    uniqueName: 'Geo',
  },
} as Meta

const Template: Story<GeoProps> = args => <Geo {...args} />

export const Preview = Template.bind({})
Preview.args = {
  position: [0, 0, 0],
}
Preview.decorators = [
  Story => (
    <PreviewLayout camera={{ zoom: 2 }}>
      <Story />
    </PreviewLayout>
  ),
]

export const Level = Template.bind({})
Level.args = {
  position: [0, 0.5, -3.5],
}
Level.decorators = [
  Story => (
    <LevelLayout>
      <Story />
    </LevelLayout>
  ),
]
