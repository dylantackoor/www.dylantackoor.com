import React, { FunctionComponent } from 'react'
import { Billboard, Text } from '@react-three/drei'

import { Geo, LevelLayout } from '@/components/canvas'

export const HelloWorldLevel: FunctionComponent = () => (
  <LevelLayout>
    <Billboard args={[2, 2]} position={[3, 1, -5]}>
      <Text color="black" position={[0, 0, 0.01]}>
        {"@dylantackoor's WebXR Experiments"}
      </Text>
    </Billboard>
    <Geo receiveShadow position={[0, 1, -5]} />
  </LevelLayout>
)
