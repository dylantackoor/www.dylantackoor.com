// TODO: Add prop to toggle Perf overlay

import React, { FunctionComponent } from 'react'
import { ContainerProps } from 'react-three-fiber'
import { VRCanvas, DefaultXRControllers, Hands } from '@react-three/xr'
import { Physics } from '@react-three/cannon'
import { Sky, PointerLockControls } from '@react-three/drei'
// import { Perf } from 'r3f-perf/dist/r3f-perf.cjs.development.js'

import { Player, Floor } from '@/components/canvas'

export const LevelLayout: FunctionComponent<ContainerProps> = ({ children, ...rest }) => (
  <VRCanvas {...rest}>
    {/* <Perf openByDefault position="top-left" trackGPU={true} /> */}

    <Physics>
      <Player />
      <PointerLockControls />
      <DefaultXRControllers />
      <Hands />

      <Sky />
      <ambientLight intensity={0.5} />
      <spotLight position={[1, 8, 1]} angle={0.3} penumbra={1} intensity={1} castShadow />

      {children}
      <Floor color="#FFF" position={[0, 0, 0]} />
    </Physics>
  </VRCanvas>
)
