import React, { FunctionComponent } from 'react'
import { ContainerProps } from 'react-three-fiber'
import { VRCanvas, DefaultXRControllers, Hands } from '@react-three/xr'
import { Physics } from '@react-three/cannon'
import { Sky, OrbitControls } from '@react-three/drei'
import { Perf } from 'r3f-perf/dist/r3f-perf.cjs.development.js'

export const PreviewLayout: FunctionComponent<ContainerProps> = ({ children, ...rest }) => (
  <VRCanvas {...rest}>
    <Perf openByDefault position="top-left" trackGPU={true} />
    <OrbitControls />

    <Physics>
      <DefaultXRControllers />
      <Hands />

      <Sky />
      <ambientLight intensity={0.5} />
      <spotLight position={[1, 8, 1]} angle={0.3} penumbra={1} intensity={1} castShadow />

      {children}
    </Physics>
  </VRCanvas>
)
