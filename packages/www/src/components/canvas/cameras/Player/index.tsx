/* eslint-disable @typescript-eslint/ban-ts-comment */

// movement example from https://codesandbox.io/s/minecraft-forked-1feep?file=/src/App.js:583-593

import React, { FunctionComponent, useEffect, useRef, useState } from 'react'
import { Vector3 } from 'three'
import { useFrame, useThree } from 'react-three-fiber'
import { SphereProps, useSphere } from '@react-three/cannon'

const SPEED = 3
const keys = { KeyW: 'forward', KeyS: 'backward', KeyA: 'left', KeyD: 'right', Space: 'jump' }
// @ts-expect-error
const moveFieldByKey = (key: any): any => keys[key]
const direction = new Vector3()
const frontVector = new Vector3()
const sideVector = new Vector3()

const usePlayerControls = () => {
  const [movement, setMovement] = useState({
    forward: false,
    backward: false,
    left: false,
    right: false,
    jump: false,
  })

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) =>
      setMovement(m => ({ ...m, [moveFieldByKey(e.code)]: true }))
    const handleKeyUp = (e: KeyboardEvent) =>
      setMovement(m => ({ ...m, [moveFieldByKey(e.code)]: false }))

    document.addEventListener('keydown', handleKeyDown)
    document.addEventListener('keyup', handleKeyUp)

    return () => {
      document.removeEventListener('keydown', handleKeyDown)
      document.removeEventListener('keyup', handleKeyUp)
    }
  }, [])

  return movement
}

export const Player: FunctionComponent<SphereProps> = props => {
  const [sphereRef, api] = useSphere(() => ({
    mass: 1,
    type: 'Dynamic',
    position: [0, 1.25, 0],
    ...props,
  }))
  const { forward, backward, left, right, jump } = usePlayerControls()
  const { camera } = useThree()
  const velocity = useRef([0, 0, 0])

  useEffect(() => void api.velocity.subscribe(v => (velocity.current = v)), [])
  useFrame(() => {
    // @ts-expect-error
    camera.position.copy(sphereRef.current.position)
    frontVector.set(0, 0, Number(backward) - Number(forward))
    sideVector.set(Number(left) - Number(right), 0, 0)
    direction
      .subVectors(frontVector, sideVector)
      .normalize()
      .multiplyScalar(SPEED)
      .applyEuler(camera.rotation)
    api.velocity.set(direction.x, velocity.current[1], direction.z)
    // @ts-expect-error
    if (jump && Math.abs(velocity.current[1].toFixed(2)) < 0.05)
      api.velocity.set(velocity.current[0], 10, velocity.current[2])
  })

  return <mesh ref={sphereRef} />
}
