import Head from 'next/head'
import React, { FunctionComponent } from 'react'

export interface SEOProps {
  title: string
}

export const SEO: FunctionComponent<SEOProps> = props => (
  <Head>
    <title>{props.title}</title>
    {props.children}
  </Head>
)
