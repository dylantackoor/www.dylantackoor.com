import { DocsPage, DocsContainer } from '@storybook/addon-docs/blocks'
import { MINIMAL_VIEWPORTS, INITIAL_VIEWPORTS } from '@storybook/addon-viewport'
// import { withTests } from '@storybook/addon-jest'

// const IS_PRODUCTION = process.env.CI

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  layout: 'fullscreen',
  viewport: {
    viewports: { ...MINIMAL_VIEWPORTS, ...INITIAL_VIEWPORTS },
  },
  docs: {
    container: DocsContainer,
    page: DocsPage,
  },
}

export const decorators = [
  // withTests({ results }),
  Story => (
    <div style={{ height: '100vh', width: '100vw' }}>
      <Story />
    </div>
  ),
]
