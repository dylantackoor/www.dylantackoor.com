const path = require('path')

module.exports = {
  stories: ['../@(src|docs)/**/*.stories.@(js|jsx|ts|tsx|mdx)'],
  addons: [
    '@storybook/addon-a11y',
    '@storybook/addon-docs',
    '@storybook/addon-design-assets',
    '@storybook/addon-viewport',
    {
      name: '@storybook/addon-storysource',
      options: {
        rule: {
          include: [path.resolve(__dirname, '../src'), path.resolve(__dirname, '../docs')],
          test: [
            /\.stories\.jsx?$/,
            /\.stories\.tsx?$/,
            // /\.stories\.mdx?$/
          ],
        },
        loaderOptions: {
          prettierConfig: { printWidth: 80, singleQuote: false },
        },
      },
    },
    // '@storybook/addon-links',
    // '@storybook/addon-essentials',
    // '@storybook/addon-jest',
  ],
  webpackFinal: async config => {
    config.resolve.alias['@'] = path.resolve(__dirname, '../src')
    return config
  },
}
