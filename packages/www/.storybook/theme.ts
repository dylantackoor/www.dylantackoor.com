import { create } from '@storybook/theming/create'

export const DylanTheme = create({
  base: 'light',
  brandTitle: 'Dylan Tackoor',
  brandUrl: 'https://dlan.me',
  enableShortcuts: false,
})
