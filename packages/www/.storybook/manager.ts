import { addons } from '@storybook/addons'

import { DylanTheme } from './theme'

addons.setConfig({
  enableShortcuts: false,
  theme: DylanTheme,
})
