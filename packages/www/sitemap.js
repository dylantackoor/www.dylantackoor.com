// FIXME

const { VERCEL_URL } = process.env

module.exports = {
  siteUrl: VERCEL_URL ? `https://${VERCEL_URL}` : 'http://localhost:3000',
  generateRobotsTxt: true,
}
