/* eslint-disable @typescript-eslint/no-var-requires */

const { ANALYZE, GOOGLE_ANALYTICS_VIEW_ID } = process.env

const { GuessPlugin } = require('guess-webpack')
const withImages = require('next-images')
const withOffline = require('next-offline')
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: ANALYZE === 'true',
})
const withTM = require('next-transpile-modules')(['three', '@react-three/xr', '@react-three/drei'])

const nextOptions = {
  reactStrictMode: true,
  poweredByHeader: false,
  productionBrowserSourceMaps: true,
  webpack: (config, { isServer }) => {
    config.module.rules.push({
      test: /\.md$/,
      loader: 'frontmatter-markdown-loader',
      options: { mode: ['react-component'] },
    })

    if (isServer && GOOGLE_ANALYTICS_VIEW_ID) {
      config.plugins.push(new GuessPlugin({ GA: GOOGLE_ANALYTICS_VIEW_ID }))
    }

    return config
  },
}

module.exports = withBundleAnalyzer(withImages(withOffline(withTM(nextOptions))))
