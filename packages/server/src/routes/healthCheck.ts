import { Request, Response } from 'express'

export const healthCheckRouter = (req: Request, res: Response): void => {
  res.send('Hello World!')
}
