import { Router } from 'express'
import { buildSchema } from 'type-graphql'
import { graphqlHTTP } from 'express-graphql'

import { UserResolver } from '../resolvers'

export const graphqlRouter = Router()

graphqlRouter.use(
  '/',
  graphqlHTTP(async () => ({
    graphiql: { headerEditorEnabled: true },
    pretty: true,
    schema: await buildSchema({
      resolvers: [UserResolver],
    }),
  })),
)
