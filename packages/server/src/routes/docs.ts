import { Router } from 'express'
import swaggerUi from 'swagger-ui-express'

const swaggerFilePath = '../../build/swagger.json'

export const swaggerRouter = Router()

swaggerRouter.use(swaggerUi.serve)
swaggerRouter.get('/', async (req, res) => {
  // TODO: only load from disk when NODE_ENV !== production
  const page = swaggerUi.generateHTML(await import(swaggerFilePath))
  return res.send(page)
})
