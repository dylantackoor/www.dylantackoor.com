import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm'
import { ObjectType, Field, ID } from 'type-graphql'

import { User } from '@dylantackoor/interfaces'

@Entity({ name: 'users' })
@ObjectType()
export class UserEntity extends BaseEntity implements User {
  @PrimaryGeneratedColumn({ unsigned: true })
  @Field(type => ID)
  id!: number

  @Column('varchar', { length: 64 })
  @Field()
  password!: string

  @Column('varchar', { length: 64, unique: true })
  @Field()
  email!: string

  @Column('varchar', { length: 64 })
  @Field()
  name!: string
}
