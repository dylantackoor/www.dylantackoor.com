import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToOne, JoinColumn } from 'typeorm'
import { ObjectType, Field, ID } from 'type-graphql'
import { User, UserProfile } from '@dylantackoor/interfaces'

import { UserEntity } from './User.entity'

@ObjectType()
@Entity({ name: 'profiles' })
export class ProfileEntity extends BaseEntity implements UserProfile {
  @Field(type => ID)
  @PrimaryGeneratedColumn({ unsigned: true })
  id!: number

  @Field()
  @Column({
    type: 'boolean',
    default: false,
  })
  prefersDarkMode!: boolean

  @Field(type => UserEntity)
  @OneToOne(() => UserEntity)
  @JoinColumn({ name: 'userID' })
  user!: User
}
