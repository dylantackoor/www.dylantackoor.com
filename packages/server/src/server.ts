import { createConnection } from 'typeorm'
import { app } from './app'

const { MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB } = process.env

createConnection({
  type: 'mysql',
  host: MYSQL_HOST,
  port: 3306,
  username: MYSQL_USER,
  password: MYSQL_PASSWORD,
  database: MYSQL_DB,
  entities: [__dirname + '/entities/*.js', __dirname + '/entities/index.ts'],
  synchronize: true,
})
  .then(() => {
    const port = process.env.PORT || '3000'
    app.listen(port, () => console.log(`listening at https://localhost:${port}`))
  })
  .catch(error => {
    console.error(error)
    throw error
  })
