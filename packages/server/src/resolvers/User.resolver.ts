import { Resolver, Query, Mutation, Arg } from 'type-graphql'
import { addUserResponse, User } from '@dylantackoor/interfaces'

import { UserEntity } from '../entities'
import { AddUserData } from '../graphql/arguments/User.args'

@Resolver(UserEntity)
export class UserResolver {
  /**
   * @description Gets a user by their ID
   * @param id user identifier
   */
  @Query(returns => UserEntity)
  async user(@Arg('id') id: string): Promise<User> {
    const user = await UserEntity.findOne(id)
    if (user === undefined) {
      throw new Error(id)
    }
    return user
  }

  @Query(returns => [UserEntity])
  async users(): Promise<User[]> {
    const users = await UserEntity.find()
    return users
  }

  @Mutation(returns => UserEntity)
  async addUser(@Arg('newUserData') newUserData: AddUserData): Promise<addUserResponse> {
    const user = UserEntity.create()
    user.email = newUserData.email
    user.name = newUserData.name
    user.password = newUserData.password
    const newUser = await user.save()

    const res: addUserResponse = {
      id: newUser.id,
      email: newUser.email,
      name: newUser.name,
    }

    return res
  }

  @Mutation(returns => Boolean)
  async removeUser(@Arg('id') id: number): Promise<boolean> {
    try {
      await UserEntity.delete({ id })
      return true
    } catch {
      return false
    }
  }
}
