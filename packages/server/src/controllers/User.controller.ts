import { Body, Controller, Example, Get, Patch, Path, Route, SuccessResponse, Tags } from 'tsoa'
import { JSONResponse, User, UserPatchRequest, UserPatchResponse } from '@dylantackoor/interfaces'

import { UserService } from '../services'

@Tags('User')
@Route('users')
export class UsersController extends Controller {
  /**
   * Retrieves the details of an existing user.
   * Supply the unique user ID and receive corresponding user details.
   *
   * For login, POST to /auth/login
   *
   * TODO: protect this route
   */
  @Get('{userId}')
  public async getUser(
    @Path() userId: number,
  ): Promise<JSONResponse<Pick<User, 'id' | 'email' | 'name'>>> {
    const res = new UserService().find(userId)
    return res
  }

  /**
   * Updates a user, profile, and/or biometrics given a userID
   * @param userId The user's identifier
   */
  @Example<UserPatchResponse>({
    data: true,
  })
  @SuccessResponse('204', 'Created')
  @Patch('{userId}')
  public async patchUser(
    @Path() userId: number,
    @Body() updateOpts: UserPatchRequest,
  ): Promise<UserPatchResponse> {
    this.setStatus(201)
    const updatedUser = await new UserService().update(userId, updateOpts)
    return updatedUser
  }
}
