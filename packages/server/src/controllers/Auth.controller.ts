import { Body, Controller, Example, Post, Route, Tags, SuccessResponse } from 'tsoa'
import {
  JSONResponse,
  UserGetRequest,
  UserGetResponse,
  UserPostRequest,
  UserPostResponse,
} from '@dylantackoor/interfaces'

import { UserService } from '../services'

@Tags('Auth')
@Route('auth')
export class AuthController extends Controller {
  /**
   * Creates a user given user info + minimum user profile info
   */
  @Example<JSONResponse<UserPostResponse>>({
    data: {
      id: 1,
    },
  })
  @SuccessResponse('201', 'Created')
  @Post()
  public async create(
    @Body() requestBody: UserPostRequest,
  ): Promise<JSONResponse<UserPostResponse>> {
    this.setStatus(201)
    const newUser = await new UserService().create(requestBody)
    return newUser
  }

  /**
   * Returns a user given correct login details.
   */
  @Example<JSONResponse<UserGetResponse>>({
    data: {
      id: 1,
      email: 'user@example.com',
      name: 'Test User',
      prefersDarkMode: true,
    },
  })
  @SuccessResponse('200', 'OK')
  @Post('login')
  public async login(@Body() loginQuery: UserGetRequest): Promise<JSONResponse<UserGetResponse>> {
    this.setStatus(201)
    const matchingUser = await new UserService().login(loginQuery)
    return matchingUser
  }
}
