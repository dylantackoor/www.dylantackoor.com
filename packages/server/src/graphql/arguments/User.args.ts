import { InputType, Field } from 'type-graphql'
import { IsEmail, MaxLength } from 'class-validator'
import { addUserData } from '@dylantackoor/interfaces'

@InputType()
export class AddUserData implements addUserData {
  @Field()
  @MaxLength(50, {
    message: 'Name is too long',
  })
  name!: string

  @Field()
  @IsEmail()
  email!: string

  @Field()
  @MaxLength(30)
  password!: string

  @Field()
  @MaxLength(30)
  passwordConfirmation!: string
}
