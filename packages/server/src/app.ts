import express, { Response as ExResponse, Request as ExRequest, NextFunction } from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import { ValidateError } from 'tsoa'

import { RegisterRoutes } from './routes'
import { healthCheckRouter, swaggerRouter, graphqlRouter } from './routes.custom'

export const app = express()

// Middlewares
app.use(express.json())
app.use(morgan('dev'))
app.use(express.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
)

// Routes
RegisterRoutes(app)
app.use('/healthcheck', healthCheckRouter)
app.use('/docs', swaggerRouter)
app.use('/graphql', graphqlRouter)
app.use((err: unknown, req: ExRequest, res: ExResponse, next: NextFunction): ExResponse | void => {
  if (err instanceof ValidateError) {
    console.warn(`Caught Validation Error for ${req.path}:`, err.fields)
    return res.status(422).json({
      message: 'Validation Failed',
      details: err?.fields,
    })
  }
  if (err instanceof Error) {
    return res.status(500).json({
      message: 'Internal Server Error',
    })
  }

  next()
})
