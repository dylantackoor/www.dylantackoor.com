import { getConnection } from 'typeorm'
import { validateEmail } from '@dylantackoor/helpers'
import {
  JSONResponse,
  User,
  UserGetRequest,
  UserGetResponse,
  UserPatchRequest,
  UserPatchResponse,
  UserPostRequest,
  UserPostResponse,
} from '@dylantackoor/interfaces'

import { UserEntity, ProfileEntity } from '../entities'

export class UserService {
  public async create(signupParams: UserPostRequest): Promise<JSONResponse<UserPostResponse>> {
    if (!validateEmail(signupParams.email)) {
      return {
        errors: [{ message: 'Invalid email address', inputName: 'email' }],
      }
    }

    // Verify matching password confirmation
    if (signupParams.password !== signupParams.passwordConfirmation) {
      return {
        errors: [
          {
            message: 'Password mismatch',
            inputName: 'password',
          },
        ],
      }
    }

    // Confirm email isn't already registered
    const existingUser = await UserEntity.findOne({ email: signupParams.email })
    if (existingUser) {
      return {
        errors: [
          {
            message: 'Email already registered',
            inputName: 'email',
          },
        ],
      }
    }

    // Create New User
    const newUser = new UserEntity()
    newUser.name = signupParams.name
    newUser.email = signupParams.email
    newUser.password = signupParams.password

    // Create new profile
    const newUserProfile = new ProfileEntity()
    newUserProfile.user = newUser

    let response: JSONResponse<UserPostResponse>
    const connection = getConnection()
    const queryRunner = connection.createQueryRunner()
    await queryRunner.startTransaction()
    try {
      const { id } = await queryRunner.manager.save(newUser)
      await queryRunner.manager.save(newUserProfile)
      await queryRunner.commitTransaction()
      response = {
        data: {
          id,
        },
      }
    } catch (err) {
      await queryRunner.rollbackTransaction()
      response = {
        errors: [{ message: 'Error saving user & profile' }, { message: err.message }],
      }
    } finally {
      await queryRunner.release()
    }

    return response
  }

  public async update(userID: number, updateOpts: UserPatchRequest): Promise<UserPatchResponse> {
    const connection = getConnection()
    const queryRunner = connection.createQueryRunner()
    let response: UserPatchResponse

    // TODO: this doesn't need to be a transaction
    await queryRunner.startTransaction()
    try {
      await queryRunner.manager.update(UserEntity, { id: userID }, updateOpts)
      await queryRunner.commitTransaction()
      response = {
        data: true,
      }
    } catch (err) {
      await queryRunner.rollbackTransaction()
      response = {
        errors: [{ message: 'Error updating user' }, { message: err }],
      }
    } finally {
      await queryRunner.release()
    }

    return response
  }

  public async login(loginQuery: UserGetRequest): Promise<JSONResponse<UserGetResponse>> {
    try {
      // TODO: combine to make a single sql query
      const existingUser = await UserEntity.findOne(loginQuery, { select: ['id', 'email', 'name'] })
      if (!existingUser) {
        return {
          errors: [
            {
              message: 'Account/Password combination not found',
            },
          ],
        }
      }

      const existingProfile = await ProfileEntity.findOne(
        { user: existingUser },
        { select: ['prefersDarkMode'] },
      )
      if (!existingProfile) {
        return {
          errors: [
            {
              message: 'Error loading profile information',
            },
          ],
        }
      }

      return {
        data: { ...existingUser, ...existingProfile },
      }
    } catch (error) {
      return {
        errors: [
          {
            message: 'Error checking for existing user or profile',
          },
          {
            message: error.message,
          },
        ],
      }
    }
  }

  public async find(userID: number): Promise<JSONResponse<User>> {
    // Confirm email isn't already registered
    const existingUser = await UserEntity.findOne(
      { id: userID },
      { select: ['email', 'id', 'name'] },
    )
    if (existingUser) {
      return {
        data: existingUser,
      }
    } else {
      return {
        errors: [
          {
            message: 'Email already registered',
            inputName: 'email',
          },
        ],
      }
    }
  }
}
