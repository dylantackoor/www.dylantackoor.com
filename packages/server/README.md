# @dylantackoor/server

An Express server for the web client to communicate with. Uses [TypeORM](https://typeorm.io/) to mange models/MariaDB, [tsoa](https://tsoa-community.github.io/docs/) to generate express routes and OpenAPI/Swagger docs, and [TypeGraphQL](https://typegraphql.com/) to generate a GraphQL schema

- Entities implementing shared interfaces with TypeORM & TypeGraphQL decorators to generate SQL and GraphQL schemas
- tsoa for generating Express routes with interface validation and OpenAPI v3 docs
- Swagger page for auto API docs + live api testing at `http://localhost:3000/docs`
- SQL web gui located at `http://localhost:8080`
- Generates a (currently unused) typescript/fetch based npm package in `../packages/api` which will remove the need to hand write REST fetch calls
- A graphiql interface on `http://localhost:3000/graphql`
- TypeUML for generating a ERD diagram of the database on each save

## TODOs

- [ ] setup graphql subscriptions
- [ ] version REST api behind a base url like `/v1`
- [ ] rename tsoa generated routes so it doesn't conflict with `./src/routes` and I can remove `./src/routes.custom.ts`
- [ ] Create local interface copies in tsoa to get stuff like omit, per their docs? doesn't feel great being forced to include my interfaces in a web server package, would prefer to merge with the graphql request types
- [ ] replace verbose error messaging by using more of <https://github.com/typestack/class-validator> if it can be reused on the frontend with treeshaking
- [ ] limit uml generation to only when saving entities?
