export type JSONResponseError = { message: string; inputName?: string }

export type JSONResponse<T> = {
  data?: T
  errors?: JSONResponseError[]
}
