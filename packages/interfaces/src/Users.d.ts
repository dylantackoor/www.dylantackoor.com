import { JSONResponse } from './Request'

// ENTITIES
export interface User {
  id: number
  password: string
  name: string
  email: string
}

export interface UserProfile {
  id: number
  prefersDarkMode: boolean
  user: User
}

// GraphQL
export type addUserData = Omit<User, 'id'> & {
  passwordConfirmation: string
}
export type addUserResponse = Omit<User, 'password'>

// REST Api
export type UserPostRequest = Pick<User, 'name' | 'email' | 'password'> & {
  passwordConfirmation: string
}
export type UserPostResponse = Pick<User, 'id'>

export type UserGetRequest = Pick<User, 'email'> & {
  password: string
}
export type UserGetResponse = Pick<User, 'id' | 'name' | 'email'> &
  Pick<UserProfile, 'prefersDarkMode'>

export type UserPatchRequest = {
  name?: string
}
export type UserPatchResponse = JSONResponse<true>
