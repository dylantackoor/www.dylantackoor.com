import { User } from './Users'

export interface Message {
  id: number
  fromUserID: User
  toUserID: User
  timestamp: Date
  content: string
  read: boolean
}
