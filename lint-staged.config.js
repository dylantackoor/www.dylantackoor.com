module.exports = {
  '*.{js,jsx,ts,tsx,d.ts,json,md,yml,yaml,css}': ['prettier --write'],
  '*.{js,jsx,ts,tsx,d.ts}': 'eslint --fix --cache --cache-location .eslintcache',
  '*.{css,jsx,tsx}': 'stylelint',
  '*.{md,mdx}': 'markdownlint --fix',
}
